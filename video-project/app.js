// MDN
// The DOMContentLoaded event fires when the initial HTML document has been completely loaded and parsed, without waiting for stylesheets, images, and subframes to finish loading.
// The load event is fired when the whole page has loaded, including all dependent resources such as stylesheets and images.


const video = document.querySelector(".video-container");

const playButton = document.querySelector(".play");

const pauseButton = document.querySelector(".pause");

const preLoader = document.querySelector(".preloader");

window.addEventListener("load", function () {
    preLoader.classList.add("hide-preloader");
});

pauseButton.addEventListener("click", function () {
    pauseButton.classList.add("animate__animated", "animate__bounceIn", "animate__repeat-2");
    video.pause();
});

playButton.addEventListener("click", function () {
    playButton.classList.add("animate__animated", "animate__bounceIn", "animate__repeat-2");
    video.play();
});
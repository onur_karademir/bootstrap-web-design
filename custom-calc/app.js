function getHistory () {
	return document.getElementById('history-value').innerText
}

function printHistory (num) {
	return document.getElementById('history-value').innerText=num
}

function getOutput () {
	return document.getElementById('output-value').innerText
}

function printOutput (num) {
	if (num == '') {
		return document.getElementById('output-value').innerText=getFormattedNumber(num)
		
	} else {
		return document.getElementById('output-value').innerText=num
	}
}
function getFormattedNumber(num) {
	let n = Number(num)
	let value = n.toLocaleString('en')
	return value
}
function reverseFormattedNumber(num) {
	return Number(num.replace(/,/g,''))
}

let operators = document.getElementsByClassName('operator')
for (let i = 0; i < operators.length; i++) {
	operators[i].addEventListener('click',function () {
		if (this.id == 'clear') {
			printHistory('')
			printOutput('')
		}
		else if (this.id == 'backspace') {
			let output = reverseFormattedNumber(getOutput()).toString()
			if (output) {
				output = output.substr(0,output.length-1)
				printOutput(output)
			}
		}
		else {
			let output = getOutput()
			let history = getHistory()
			if (output != '') {
				output = reverseFormattedNumber(output)
				history=history+output
				if (this.id == '=') {
					let result = eval(history)
					printOutput(result)
					printHistory('')
				}
				else{
					history = history+this.id
					printHistory(history)
					printOutput('')
				}
			} 
		}
	})
}
let allNumbers = document.getElementsByClassName('number')
for (let i = 0; i < allNumbers.length; i++) {
	allNumbers[i].addEventListener('click',function () {
		let output = reverseFormattedNumber(getOutput())
		if (output!=NaN) {
			output = output + this.id
			printOutput(output)
		}
	})
}
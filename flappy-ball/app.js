var block = document.getElementById("block");
var blockTwo = document.getElementById("block2");
var hole = document.getElementById("hole");
var holeTwo = document.getElementById("hole2");
var character = document.getElementById("character");
var body = document.getElementById("body");
var background = document.getElementById("game");
var jumping = 0;
var counter = 0;

window.addEventListener('click', () => {
    const audio = document.querySelector('.audio');
    audio.play()
});


hole.addEventListener('animationiteration',()=>{
    var random = -((Math.random()*300)+150);
    hole.style.top = random + "px";
    counter ++
});

setInterval(function(){
var characterTop = parseInt(window.getComputedStyle(character).getPropertyValue('top'));
if(jumping==0){
    character.style.top = (characterTop+3)+"px";
}
var blockLeft = parseInt(window.getComputedStyle(block).getPropertyValue('left'));
var holeTop = parseInt(window.getComputedStyle(hole).getPropertyValue('top'));
var cTop = -(500-characterTop);

if((characterTop > 480)||((blockLeft<20)&&(blockLeft>-50)&&((cTop<holeTop)||(cTop>holeTop+130)))){
    const audioDown = document.querySelector('.audio2');
    audioDown.play();
    audioDown.currentTime = 0;
   alert('game over.score:'+ counter );
   character.style.top = 100 + 'px';
   counter = 0;
   background.className = "";
   background.classList.add("animatedBackground")
}
if (counter > 1) {
    // console.log("level 1");
    background.classList.remove("animatedBackground");
    background.classList.add("animatedBackground2");
}
if (counter > 2) {
    // console.log("level 2");
    background.classList.remove("animatedBackground2");
    background.classList.add("animatedBackground3");
}
if (counter > 3) {
    // console.log("level 3");
    background.classList.remove("animatedBackground3");
    background.classList.add("animatedBackground4");
}
if (counter > 4) {
    // console.log("level 4");
    background.classList.remove("animatedBackground4");
    background.classList.add("animatedBackground5");
}
if (counter > 5) {
    // console.log("level 5");
    background.classList.remove("animatedBackground5");
    background.classList.add("animatedBackground6");
}
},10);
function jump(){
    jumping = 1;
    let jumpCount = 0;
    var jumpInterval = setInterval( function () {
        var characterTop = parseInt(window.getComputedStyle(character).getPropertyValue('top'));
        if ((characterTop > 6)&&(jumpCount < 15)) {
            character.style.top = (characterTop-5)+"px";
        }
        if(jumpCount > 20){
            clearInterval(jumpInterval);
            jumping = 0;
            jumpCount = 0;
        }
        jumpCount ++
    }, 10);
}
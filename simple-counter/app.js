

let btn = document.querySelectorAll('.btn');

let value = document.getElementById('value');

let count = 0 ;


btn.forEach(function (btn){
    btn.addEventListener('click',function(e){
        const styles = e.currentTarget.classList;
        if (styles.contains('increase')) {
            count ++
        } else if(styles.contains('decrease')){
            count --
        }
        else{
            count = 0;
        }
        if (count > 0) {
            value.style.color = 'green'
        }else if(count < 0){
            value.style.color = 'red'
        } 
        else {
            value.style.color= '#343a40'
        }
        value.textContent = count
    });
});
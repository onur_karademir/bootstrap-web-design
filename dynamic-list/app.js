// ****** SELECT ITEMS **********
const form = document.querySelector(".main-form");

const deleteAllBtn = document.querySelector(".delete-all-btn");

const itemList = document.querySelector(".item-list");

const submitBtn = document.querySelector(".submit-btn");

const input = document.getElementById("input-id");

const alert = document.querySelector(".my-alert");

// Edit Option //
let editElement;
let editFlag = false;
let editId = "";
// ****** EVENT LISTENERS **********
form.addEventListener("submit", submitHendler);
deleteAllBtn.addEventListener("click", clearAllItems);
// ****** FUNCTIONS **********
if (itemList.children.length === 0) {
  deleteAllBtn.classList.remove("show-delete-button");
}
function submitHendler(e) {
  e.preventDefault();
  const value = input.value;
  const id = new Date().getTime().toString();

  if (value !== "" && editFlag === false) {
    const element = document.createElement("article");
    const attr = document.createAttribute("data-id");
    attr.value = id;
    element.setAttributeNode(attr);
    element.classList.add("dynamic-item", "mb-3");
    element.innerHTML = `
    <p class="title">${value}</p>
      <div class="btn-container">
          <button class="btn btn-success btn-sm edit-btn">Edit</button>
          <button class="btn btn-danger btn-sm delete-btn">Delete</button>
          </div>
    `;
    itemList.appendChild(element);
    deleteAllBtn.classList.add("show-delete-button");
    displayAlert("Item Added", "success");
    setBackDefault()
    const editBtn = document.querySelector(".edit-btn");
    const deleteBtn = document.querySelector(".delete-btn");
    deleteBtn.addEventListener("click", deleteItems);
    editBtn.addEventListener("click", editItems);
  } else if (value !== "" && editFlag === true) {
    editElement.innerHTML = value;
    displayAlert("Item Edited", "warning");setBackDefault()
  } else {
  }
}
function displayAlert(text, alerColor) {
  alert.innerText = text;
  alert.classList.add(`my-alert-${alerColor}`);
  setTimeout(function () {
    alert.innerText = "";
    alert.classList.remove(`my-alert-${alerColor}`);
  }, 1500);
}
function clearAllItems(e) {
  const element = document.querySelectorAll(".dynamic-item");
  if (element.length > 0) {
    element.forEach(function (item) {
      itemList.removeChild(item);
    });
  }
  
  deleteAllBtn.classList.remove("show-delete-button");
  displayAlert("All Item Deleted", "danger");
  setBackDefault()
}
function deleteItems(e) {
  const element = e.currentTarget.parentElement.parentElement;
  itemList.removeChild(element);
  if (itemList.children.length === 0) {
    deleteAllBtn.classList.remove("show-delete-button");
  }
  displayAlert("Item Deleted", "warning");
  setBackDefault()
}
function editItems(e) {
  const element = e.currentTarget.parentElement.parentElement;
  editElement = e.currentTarget.parentElement.previousElementSibling;
  editFlag = true;
  input.value = editElement.innerHTML;
  editId = element.dataset.id;
  submitBtn.textContent = "edit";
}
function setBackDefault() {
  editElement;
  editFlag = false;
  editId = "";
  submitBtn.textContent = "Add Item";
}
// ****** LOCAL STORAGE **********

// ****** SETUP ITEMS **********


// function submitHendler(e) {
//   e.preventDefault();
//   const value = input.value;
//   const id = new Date().getTime().toString();
//   if (value !== "" && editFlag === false) {
//     const element = document.createElement("article");
//     const attr = document.createAttribute("data-id");
//     attr.value = id;
//     element.classList.add("dynamic-item", "mb-3");
//     element.innerHTML = `<p class="title">${value}</p>
//     <div class="btn-container">
//         <button class="btn btn-success btn-sm edit-btn">Edit</button>
//         <button class="btn btn-danger btn-sm delete-btn">Delete</button>
//         </div>
//     `;
//     itemList.appendChild(element);
//     displayAlert("Item Added", "success");
//     const editBtn = document.querySelector(".edit-btn");
//     const deleteBtn = document.querySelector(".delete-btn");
//     deleteBtn.addEventListener("click", deleteItems);
//     editBtn.addEventListener("click", editItems);
//     setBackDefaultSettings();
//   } else if (value !== "" && editFlag === true) {
//     editElement.innerHTML = value;
//     displayAlert("Item Added", "warning");
//     setBackDefaultSettings();
//   } else {
//   }
// }
// function displayAlert(text, alertColor) {
//   alert.innerText = text;
//   alert.classList.add(`my-alert-${alertColor}`);
//   setTimeout(function () {
//     alert.innerText = "";
//     alert.classList.remove(`my-alert-${alertColor}`);
//   }, 1500);
// }
// function allDeleteItems(e) {
//   const element = document.querySelectorAll(".dynamic-item");
//   if (element.length > 0) {
//     element.forEach(function (item) {
//       itemList.removeChild(item);
//     });
//   }
//   displayAlert("All Item Deleted", "danger");
//   setBackDefaultSettings();
// }
// function deleteItems(e) {
//   const element = e.currentTarget.parentElement.parentElement;
//   const id = element.dataset.id;
//   itemList.removeChild(element);
//   displayAlert("All Item Deleted", "danger");
//   setBackDefaultSettings();
// }
// function editItems(e) {
//   const element = e.currentTarget.parentElement.parentElement;
//   editElement = e.currentTarget.parentElement.previousElementSibling;
//   editFlag = true;
//   input.value = editElement.innerHTML;
//   editId = element.dataset.id;
//   submitBtn.textContent = "edit";
// } 

// function setBackDefaultSettings() {
//   form.value = "";
//   editFlag = false;
//   editId = "";
//   submitBtn.textContent = "Add Item";
// }